package eu.proximate.sl.base.factory;


import eu.proximate.sl.base.restaurant.server.RestaurantServiceImpl;
import eu.proximate.sl.base.restaurant.shared.RestaurantService;
import eu.proximate.sl.base.service.BaseServiceable;

public class ServiceFactory<T> {
	
	private static final RestaurantService restaurantService = new RestaurantServiceImpl();
	
	
	public static <T extends BaseServiceable>T getService(Class<T> service){
		if(service.equals(RestaurantService.class)){
			return (T) restaurantService;
		}
		return null;
	}

}
