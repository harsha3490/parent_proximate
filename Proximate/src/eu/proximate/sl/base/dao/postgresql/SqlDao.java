package eu.proximate.sl.base.dao.postgresql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import eu.proximate.sl.base.dao.BaseDao;
import eu.proximate.sl.base.utils.Constants;


public abstract class SqlDao extends BaseDao {
	
	protected static String DATABASE_NAME=Constants.DATABASE_NAME;
	
	@Override
	protected void initializeDataSource() {
		if(null==dataSource){
		try{
		Context initContext = new InitialContext();
		Context envContext  = (Context)initContext.lookup("java:/comp/env");
		dataSource = (DataSource)envContext.lookup("jdbc/ProximateDB");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		}
	}
	
	protected void closeAll(ResultSet rs,Statement stmt,Connection con){
		if(null!=rs){
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		if(null!=stmt){
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		if(null!=con){
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
