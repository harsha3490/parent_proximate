package eu.proximate.sl.base.dao;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

public abstract class BaseDao {

	protected static DataSource dataSource;
	
	protected abstract void initializeDataSource();
	
	
	/** 
	 * This method is used to get connection from cached datasource object. Always check for null before using connection
	 */
	protected Connection getConnection(){
		initializeDataSource();
		if(null!=dataSource){
			try {
				return dataSource.getConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	
}
