package eu.proximate.sl.base.restaurant.shared;

import eu.proximate.sl.base.service.BaseServiceable;

public interface RestaurantService extends BaseServiceable {

	public String getDetailsUsingUserName(String userName);
	public String getDetailsUsingEmailID(String emailId);
	

}
