package eu.proximate.sl.base.restaurant.server;

import eu.proximate.sl.base.restaurant.server.dao.RestaurantDAO;
import eu.proximate.sl.base.restaurant.shared.RestaurantService;

public class RestaurantServiceImpl implements RestaurantService{

	@Override
	public String getDetailsUsingUserName(String userName) {
		return RestaurantDAO.getInstance().getDetailsUsingUserName(userName);
	}

	@Override
	public String getDetailsUsingEmailID(String emailId) {
		// TODO Auto-generated method stub
		return null;
	}

}
