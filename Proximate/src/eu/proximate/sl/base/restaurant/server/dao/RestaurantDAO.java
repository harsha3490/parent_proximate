package eu.proximate.sl.base.restaurant.server.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import eu.proximate.sl.base.dao.postgresql.SqlDao;
import eu.proximate.sl.base.utils.CommonUtils;

public class RestaurantDAO extends SqlDao{

	private static final RestaurantDAO INSTANCE = new RestaurantDAO();

	public static RestaurantDAO getInstance() {
		return INSTANCE;
	}
	
	private RestaurantDAO() {
		
	}

	public String getDetailsUsingUserName(String userName) {
		Connection con = getConnection();
		ResultSet rs = null;
		PreparedStatement ps=null;
		String sql = "SELECT USER_NAME,PWD,EMAIL FROM USERS WHERE USER_NAME=?";
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1,userName);
			rs = ps.executeQuery();
			ResultSetMetaData  rsMetaData = rs.getMetaData();
			JsonArray array = new JsonArray();
			while(rs.next()){
				JsonObject jsonObject = new JsonObject();
				for (int i=1;i<=rsMetaData.getColumnCount();i++) {
				jsonObject.addProperty(CommonUtils.getCamelCaseString(rs.getMetaData().getColumnName(i)), rs.getString(rs.getMetaData().getColumnName(i)));
				}
				array.add(jsonObject);
			}
			return array.toString();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			closeAll(rs,ps, con);
		}
		
		return "";
	}
	
	

}
