package eu.proximate.sl.base.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.proximate.sl.base.factory.ServiceFactory;
import eu.proximate.sl.base.restaurant.shared.RestaurantService;
import eu.proximate.sl.base.service.rest.RestBaseService;

@Path("/restaurant")
public class RestaurantRestService extends RestBaseService implements RestaurantService {

	@Override
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/detailsUsingUserName")
	public String getDetailsUsingUserName(@QueryParam("userName") String userName) {
		RestaurantService restaurantService = ServiceFactory.getService(RestaurantService.class);
		return restaurantService.getDetailsUsingUserName(userName);
	}

	@Override
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/detailsUsingEmail")
	public String getDetailsUsingEmailID(@QueryParam("emailId") String emailId) {
		RestaurantService restaurantService = ServiceFactory.getService(RestaurantService.class);
		return restaurantService.getDetailsUsingEmailID(emailId);
	}

}
