package eu.proximate.sl.base.utils;

public class Constants {
	
	public static final String ASSIGNMENTS = "Assignments";
	public static final String ATTENDANCE = "Attendance";
	public static final String FEES = "Fees";
	public static final String HOLIDAYS = "Holidays";
	public static final String EVENTS = "Events";
	public static final String REMARKS = "Remarks";
	public static final String PENDING = "Pending";
	public static final String HOLIDAY = "Holiday";
	public static final String WEEKOFF = "Week off";
	public static final String PRESENT = "Present";
	public static final String ABSENT = "Absent";
	public static final String NOTUPDATED = "Not updated";
	public static final String ATTENDANCE_TIME = "17:00";
	public static final String DATABASE_NAME = "harvest1_sma";
	public static final String SUCCESS = "SUCCESS";

}
