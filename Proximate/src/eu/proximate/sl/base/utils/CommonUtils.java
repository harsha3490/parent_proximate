package eu.proximate.sl.base.utils;

public class CommonUtils {

	public static String getCamelCaseString(String columnName) {
		String camelCaseString = null;
		if(null != columnName && !columnName.isEmpty()){
			String[] str = columnName.split("_");
			if(str.length > 1){
				for (int i=0;i<str.length;i++) {
					if(i==0){
						camelCaseString = str[0].toLowerCase();
					}
					else
						camelCaseString = camelCaseString+str[i].charAt(0)+str[i].substring(1, str[i].length()).toLowerCase();
				}
			}
			else{
				camelCaseString = str[0].toLowerCase();
			}
		}
		return camelCaseString;
	}



}
